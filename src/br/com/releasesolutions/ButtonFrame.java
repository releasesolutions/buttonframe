package br.com.releasesolutions;

// Figura 12.15: ButtonFrame.java
// Bot�es de comando e eventos de a��o.

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonFrame extends JFrame {

    private final JButton plainButton; // bot�o apenas com texto
    private final JButton fancyButton; // bot�o com �cones

    // ButtonFrame adiciona JButtons ao JFrame
    public ButtonFrame() {
        super("Testing Buttons");
        setLayout(new FlowLayout());

        plainButton = new JButton("Plain Button"); // bot�o com texto
        add(plainButton); // adiciona plainButton ao JFrame

        Icon bug1 = new ImageIcon(getClass().getResource("images/btn1.png"));
        Icon bug2 = new ImageIcon(getClass().getResource("images/btn2.png"));
        fancyButton = new JButton("Fancy Button", bug1); // configura imagem
        fancyButton.setRolloverIcon(bug2);
        add(fancyButton); // adiciona fancyButton ao JFrame

        // cria novo ButtonHandler de tratamento para tratamento de evento de bot�o
        ButtonHandler handler = new ButtonHandler();
        fancyButton.addActionListener(handler);
        plainButton.addActionListener(handler);
    }

    // classe interna para tratamento de evento de bot�o
    private class ButtonHandler implements ActionListener {

        // trata evento de bot�o
        @Override
        public void actionPerformed(ActionEvent event) {

            JOptionPane.showMessageDialog(ButtonFrame.this, String.format("You pressed: %s", event.getActionCommand()));
        }
    }
} // fim da classe ButtonFrame
